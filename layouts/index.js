import Footer from './HomeLayout/Footer/Footer';
import Navbar from './HomeLayout/Header/Header';

const Layout = ({ children }) => {
  return (
    <>
      <div className=''>
        <Navbar />
        <main className=''>{children}</main>
        <Footer />
      </div>
    </>
  );
};

export default Layout;
