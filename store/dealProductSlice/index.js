const { createSlice } = require('@reduxjs/toolkit');

const initialProduct = {
  products: [
    {
      id: 1,
      productImg: 'https://i.ibb.co/6bj0DpJ/deal-Product1.png',
      productName: 'Watermelon',
      currentPrice: 250,
      previousPrice: 350,
      ratings: 5,
      numOfRatings: 126,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
    {
      id: 2,
      productImg: 'https://i.ibb.co/wMrngnP/deal-Product2.png',
      productName: 'Pineapple',
      currentPrice: 178,
      previousPrice: 274,
      ratings: 5,
      numOfRatings: 124,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
    {
      id: 3,
      productImg: 'https://i.ibb.co/0rvZx8g/deal-Product3.png',
      productName: 'Tomatoo Sausce',
      currentPrice: 289,
      previousPrice: 349,
      ratings: 5,
      numOfRatings: 96,
      category: 'groceries',
      preSubCategory: 'sausce',
      subCategory: 'tomatoo sausce',
      numOfCartProduct: 1,
    },
    {
      id: 4,
      productImg: 'https://i.ibb.co/kxb1dbB/deal-Product4.png',
      productName: 'Capsicum',
      currentPrice: 335,
      previousPrice: 452,
      ratings: 5,
      numOfRatings: 183,
      category: 'vegetables',
      preSubCategory: 'green vegetables',
      subCategory: 'fresh vegetables',
      numOfCartProduct: 1,
    },
    {
      id: 5,
      productImg: 'https://i.ibb.co/1GHh67X/deal-Product5.png',
      productName: 'Burmese grape',
      currentPrice: 231,
      previousPrice: 456,
      ratings: 5,
      numOfRatings: 123,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
    {
      id: 6,
      productImg: 'https://i.ibb.co/6bSwqqP/deal-Product6.png',
      productName: 'Apple',
      currentPrice: 258,
      previousPrice: 369,
      ratings: 5,
      numOfRatings: 147,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
    {
      id: 7,
      productImg: 'https://i.ibb.co/bXqh4wM/deal-Product7.png',
      productName: 'Mango',
      currentPrice: 90,
      previousPrice: 125,
      ratings: 5,
      numOfRatings: 151,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
    {
      id: 8,
      productImg: 'https://i.ibb.co/9YMWPX1/deal-Product8.png',
      productName: 'Lychee',
      currentPrice: 300,
      previousPrice: 400,
      ratings: 5,
      numOfRatings: 111,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
    {
      id: 9,
      productImg: 'https://i.ibb.co/N1b8wMC/deal-Product9.png',
      productName: 'Pomegranate',
      currentPrice: 350,
      previousPrice: 420,
      ratings: 5,
      numOfRatings: 181,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
    {
      id: 10,
      productImg: 'https://i.ibb.co/TYhk9vY/deal-Product10.png',
      productName: 'Guava',
      currentPrice: 150,
      previousPrice: 1800,
      ratings: 5,
      numOfRatings: 164,
      category: 'fruits',
      preSubCategory: 'green fruits',
      subCategory: 'fresh fruits',
      numOfCartProduct: 1,
    },
  ],
  cart: [],
  totalPrice: 0,
};
const dealProductsSlice = createSlice({
  name: 'dealProducts',
  initialState: initialProduct,
  reducers: {
    allProducts: (state, action) => {
      state.products = action.payload;
    },
    increment: (state, action) => {
      const id = action.payload;
      // const findProduct = state.products.find(product=>product.id===id);
      // console.log('findProduct',findProduct);
      const index = state.products.findIndex((product) => product.id === id);
      // console.log('index',index);
      state.products[index].numOfCartProduct =
        state.products[index].numOfCartProduct + 1;
    },
    decrement: (state, action) => {
      const id = action.payload;

      const index = state.products.findIndex((product) => product.id === id);
      if (state.products[index].numOfCartProduct > 1) {
        state.products[index].numOfCartProduct =
          state.products[index].numOfCartProduct - 1;
      }
    },
    addToCart: (state, action) => {
      // state.cart.push(action.payload);
      const product = action.payload;
      // console.log(product);
      const productId = product.id;
      if (state.cart && productId) {
        if (state.cart.find((item) => item.id == productId)) {
          const index = state.cart.findIndex(
            (product) => product.id === productId
          );
          state.cart[index].numOfCartProduct =
            state.cart[index].numOfCartProduct + 
            state.products[index].numOfCartProduct;
          // const n = state.products[index].numOfCartProduct;
          // console.log('cart amount', n);
        } else {
          state.cart.push(action.payload);
        }
      }
    },
    removeFromCart: (state, action) => {
      state.cart = state.cart.filter(
        (product) => product.id !== action.payload
      );
    },
  },
});

export const {
  allProducts,
  increment,
  decrement,
  addToCart,
  totalPrice,
  removeFromCart,
} = dealProductsSlice.actions;

export default dealProductsSlice.reducer;
