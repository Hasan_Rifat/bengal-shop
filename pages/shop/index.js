import ShopMain from '../../components/subpages/Shop';
import { useDispatch } from 'react-redux';
import { allShopProducts } from '../../store/shopProductSlice';
import { useEffect } from 'react';

const Shop = ({ products }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(allShopProducts(products));
  }, [products, dispatch]);

  return (
    <div className=''>
      <ShopMain />
    </div>
  );
};

export default Shop;

export async function getServerSideProps() {
  const response = await fetch(
    'https://next-js-server-puce.vercel.app/api/shopProduct'
  );
  const data = await response.json();

  return {
    props: {
      products: data,
    },
  };
}
