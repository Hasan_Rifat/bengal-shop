import React from 'react';
import CheckoutIndex from '../../../components/subpages/checkout';

const CheckoutOrder = () => {
  return (
    <div>
      <CheckoutIndex />
    </div>
  );
};

export default CheckoutOrder;
