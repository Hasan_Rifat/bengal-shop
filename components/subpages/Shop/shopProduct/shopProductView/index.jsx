import Image from 'next/image';
import { IoIosAdd } from 'react-icons/io';
import { IoIosRemove } from 'react-icons/io';
import { AiOutlineStar } from 'react-icons/ai';
import { BsArrowRightShort } from 'react-icons/bs';
import { addToCart } from '../../../../../store/dealProductSlice';
import { useSelector, useDispatch } from 'react-redux';
import { decrement, increment } from '../../../../../store/shopProductSlice';

const ShopProductView = ({ product }) => {
  const cart = useSelector((state) => state.dealProductsReducer.cart);
  const products = useSelector(
    (state) => state.shopProductsReducer.copyOfProducts
  );
  // console.log(products);
  const dispatch = useDispatch();
  const {
    id,
    productImg,
    productName,
    category,
    currentPrice,
    previousPrice,
    subCategory,
    preSubCategory,
    ratings,
    numOfRatings,
    numOfCartProduct,
  } = product;

  const handleIncrement = (id) => {
    // console.log('clicked', id);
    dispatch(increment(id));
  };

  const handleAddToCart = (product) => {
    const productId = product.id;
    if (cart.find((item) => item.id === productId)) {
      // const index = cart.findIndex((product) => product.id === productId);
      // console.log('index',index);
      // cart[index].numOfCartProduct =
      //   cart[index].numOfCartProduct + 1;
      dispatch(addToCart(product));
    } else {
      dispatch(addToCart(product));
    }
  };

  return (
    <div className='rounded overflow-hidden group m-2'>
      <div className='relative'>
        <Image
          width={405}
          height={405}
          // layout='fill'
          src={productImg}
          alt='image not found'
          className='object-cover'
        ></Image>
        <div className='absolute inset-0 bg-black bg-opacity-40 flex items-center justify-center gap-2 opacity-0 group-hover:opacity-100 transition'>
          <button
            href='#'
            className='text-white text-lg w-9 h-9 rounded-full border-2 border-white flex items-center justify-center  transition'
            onClick={() => {
              dispatch(decrement(id));
            }}
          >
            <i className='text-white text-lg '>
              <IoIosRemove />
            </i>
          </button>
          <button
            href='#'
            className='text-green-500 text-lg font-semibold w-9 h-9 rounded-full bg-white flex items-center justify-center  transition'
          >
            {numOfCartProduct}
          </button>
          <button
            href='#'
            className='text-white text-lg w-9 h-9 rounded-full  border-2 border-white flex items-center justify-center transition'
            onClick={() => handleIncrement(id)}
          >
            <i>
              <IoIosAdd />
            </i>
          </button>
          <button
            href='#'
            className='text-white text-lg px-6 py-1.5 rounded-full absolute top-[60%] bg-[#27AE60] hover:bg-green-500'
            onClick={() => handleAddToCart(product)}
          >
            Add to Cart
          </button>
          <div className='absolute p-2 bottom-0 bg-[#F2F2F2] left-0 right-0'>
            <button className='flex items-center ml-[35%]'>
              <p className='text-[15px] text-zinc-400'>Details</p>
              <i className='text-[20px] text-zinc-400'>
                <BsArrowRightShort />
              </i>
            </button>
          </div>
        </div>
      </div>
      <div className='pt-4 pb-3 px-4 text-center space-y-2'>
        <div className='flex items-center mb-1 space-x-2 justify-center'>
          <p className='text-sm text-gray-400'>{category}</p>
        </div>

        <div className='flex items-center justify-center'>
          <div className='flex gap-1 text-sm text-yellow-400'>
            <span>
              <i>
                <AiOutlineStar />
              </i>
            </span>
            <span>
              <i>
                <AiOutlineStar />
              </i>
            </span>
            <span>
              <i>
                <AiOutlineStar />
              </i>
            </span>
            <span>
              <i>
                <AiOutlineStar />
              </i>
            </span>
            <span>
              <i>
                <AiOutlineStar />
              </i>
            </span>
          </div>
          <div className='text-xs text-gray-500 ml-2'>{numOfRatings}</div>
        </div>
        <div className='flex items-center mb-1 space-x-2 justify-center'>
          <p className='text-sm text-black font-semibold'>{productName}</p>
        </div>
        <div className='flex items-center mb-1 space-x-2 justify-center'>
          <p className='text-lg text-[#FF5C00] font-semibold'>
            ${currentPrice}
          </p>
          <p className='text-sm text-gray-400 line-through'>${previousPrice}</p>
        </div>
      </div>
    </div>
  );
};

export default ShopProductView;
