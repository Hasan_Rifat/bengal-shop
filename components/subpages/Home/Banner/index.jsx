import BannerImage from '../../../../public/images/hero_bg.png';
import Image from 'next/image';

const Banner = () => {
  return (
    <div className='container mx-auto px-4'>
      <div className='relative z-10'>
        <Image
          src={BannerImage}
          alt='image not found'
          className='object-cover rounded'
        ></Image>
      </div>
    </div>
  );
};

export default Banner;
