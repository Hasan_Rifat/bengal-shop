import Image from 'next/image';
import { useRef } from 'react';

import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { AiOutlineLeft } from 'react-icons/ai';
import { AiOutlineRight } from 'react-icons/ai';

import product1 from '../../../../public/images/fashion1.png';
import product2 from '../../../../public/images/fashion2.png';
import product3 from '../../../../public/images/fashion3.png';
import product4 from '../../../../public/images/fashion4.png';
import product5 from '../../../../public/images/fashion5.png';
import product6 from '../../../../public/images/fashion6.png';
import ProductInfo from './ProductInfo';

const productData = [
  {
    img: product1,
    name: 'Grocery',
    id: 1,
  },
  {
    img: product2,
    name: 'Fish',
    id: 2,
  },
  {
    img: product3,
    name: 'Meat',
    id: 3,
  },
  {
    img: product4,
    name: 'Vegetable',
    id: 4,
  },
  {
    img: product5,
    name: 'Fruits',
    id: 5,
  },
  {
    img: product6,
    name: 'Ice Cream',
    id: 6,
  },
  {
    img: product1,
    name: 'Grocery',
    id: 7,
  },
  {
    img: product2,
    name: 'Fish',
    id: 8,
  },
  {
    img: product3,
    name: 'Meat',
    id: 9,
  },
  {
    img: product4,
    name: 'Vegetable',
    id: 10,
  },
  {
    img: product5,
    name: 'Fruits',
    id: 11,
  },
  {
    img: product6,
    name: 'Ice Cream',
    id: 12,
  },
];

const ProductSlide = () => {
  const sliderRef = useRef(null);
  const settings = {
    slidesToShow: 6,
    arrows: false,
    responsive: [
      {
        breakpoint: 411,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 540,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 640,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 766,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 1023,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1278,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 1400,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 5,
        },
      },
      {
        breakpoint: 1920,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 5,
        },
      },
      {
        breakpoint: 2230,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 7,
        },
      },
      {
        breakpoint: 2550,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 8,
        },
      },
    ],
  };
  return (
    <div className='container mx-auto mt-1 px-4'>
      <div className='flex items-center justify-center'>
        <div
          className=' basis-[50px] ml-4 w-[50px] h-[50px] bg-green-500 rounded-full relative -left-5'
          onClick={() => sliderRef.current.slickPrev()}
        >
          <i className='text-white absolute top-[32%] left-[28%] text-xl font'>
            <AiOutlineLeft />
          </i>
        </div>
        <div className='smd:w-[70%] md:w-[80%] w-[65%] sm:w-[80%] xs:w-[50%] mx-auto'>
          <Slider ref={sliderRef} {...settings}>
            {productData.map((product) => (
              <ProductInfo key={product.id} product={product}></ProductInfo>
            ))}
          </Slider>
        </div>
        <div
          className='basis-[50px] w-[50px] h-[50px] bg-green-500 rounded-full relative'
          onClick={() => sliderRef.current.slickNext()}
        >
          <i className='text-white absolute top-[32%] left-[30%] text-xl'>
            <AiOutlineRight />
          </i>
        </div>
      </div>
    </div>
  );
};

export default ProductSlide;
