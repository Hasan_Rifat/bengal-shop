import Image from 'next/image';

const ProductInfo = ({ product }) => {
  // console.log(product);
  const { img, name } = product;
  return (
    <div className='border-solid border-2 border-gray-200 rounded-full w-[180px] h-[180px] md:w-[180px] md:h-[180px] mt-12 mb-6 relative'>
      <div className='flex items-center justify-center absolute left-[20%] top-[16%]'>
        <Image
          width={110}
          src={img}
          alt='image not found'
          className='object-cover '
        ></Image>
      </div>
    </div>
  );
};

export default ProductInfo;
