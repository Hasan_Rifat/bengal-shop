import { useRef } from 'react';
import { AiOutlineLeft } from 'react-icons/ai';
import { AiOutlineRight } from 'react-icons/ai';

import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import img1 from '../../../../public/images/b1.PNG';
import img2 from '../../../../public/images/b2.PNG';
import img3 from '../../../../public/images/b3.PNG';
import img4 from '../../../../public/images/b4.PNG';
import img5 from '../../../../public/images/b5.PNG';
import img6 from '../../../../public/images/b6.PNG';
import Image from 'next/image';

const brandData = [
  {
    img: img1,
    id: 1,
  },
  {
    img: img2,
    id: 2,
  },
  {
    img: img3,
    id: 3,
  },
  {
    img: img4,
    id: 4,
  },
  {
    img: img5,
    id: 5,
  },
  {
    img: img6,
    id: 6,
  },
  {
    img: img1,
    id: 7,
  },
  {
    img: img2,
    id: 8,
  },
  {
    img: img3,
    id: 9,
  },
  {
    img: img4,
    id: 10,
  },
  {
    img: img5,
    id: 11,
  },
  {
    img: img6,
    id: 12,
  },
];

const Brands = () => {
  const sliderRef = useRef(null);

  const settings = {
    slidesToShow: 6,
    arrows: false,
    responsive: [
      {
        breakpoint: 411,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 540,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 640,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 1280,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 5,
        },
      },
      {
        breakpoint: 1400,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 5,
        },
      },
      {
        breakpoint: 1920,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 6,
        },
      },
    ],
  };
  return (
    <div className='container mx-auto mt-16 mb-10 px-4'>
      <div className='flex justify-between items-center mb-4'>
        <div className=''>
          <h1 className='text-2xl font-bold'>Popular Brands</h1>
        </div>
        <div className='flex justify-around mr-2'>
          <div
            className='rounded-full w-[50px] h-[50px] bg-green-500 flex items-center justify-center mr-3'
            onClick={() => sliderRef.current.slickPrev()}
          >
            <i className='text-white text-[30px]'>
              <AiOutlineLeft />
            </i>
          </div>
          <div
            className='rounded-full w-[50px] h-[50px] bg-green-500 flex items-center justify-center'
            onClick={() => sliderRef.current.slickNext()}
          >
            <i className='text-white text-[30px]'>
              <AiOutlineRight />
            </i>
          </div>
        </div>
      </div>
      <div className='relative'>
        <div className='border-2 border-green-500 w-[60px] rounded absolute top-0'></div>
        <div className='border border-gray-200 rounded'></div>
      </div>

      <div className=''>
        <Slider ref={sliderRef} {...settings}>
          {brandData.map((brand) => (
            <div className='mt-1' key={brand.id}>
              <Image
                src={brand.img}
                alt='image not found'
                className='object-cover'
              ></Image>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default Brands;
