import { useRef } from 'react';
import { AiOutlineLeft } from 'react-icons/ai';
import { AiOutlineRight } from 'react-icons/ai';

const DealsProduct = () => {
  const sliderRef = useRef(null);
  
  return (
    <div className='container mx-auto mb-4'>
      <div className='flex justify-between items-center mb-4'>
        <div className=''>
          <h1 className='text-2xl font-bold'>Deals of the Week</h1>
        </div>
        <div className='flex justify-around mr-2'>
          <div
            className='rounded-full w-[50px] h-[50px] bg-green-500 flex items-center justify-center mr-3'
            onClick={() => sliderRef.current.slickPrev()}
          >
            <i className='text-white text-[30px]'>
              <AiOutlineLeft />
            </i>
          </div>
          <div
            className='rounded-full w-[50px] h-[50px] bg-green-500 flex items-center justify-center'
            onClick={() => sliderRef.current.slickNext()}
          >
            <i className='text-white text-[30px]'>
              <AiOutlineRight />
            </i>
          </div>
        </div>
      </div>
      <div className='relative'>
        <div className='border-2 border-green-500 w-[60px] rounded absolute top-0'></div>
        <div className='border border-gray-200 rounded'></div>
      </div>
    </div>
  );
};

export default DealsProduct;
