import Layout from '../../../layouts';
import CartShopping from './cartShopping';

const CartItems = () => {

  return (
    <div>
      <Layout>
        <CartShopping/>
      </Layout>
    </div>
  );
};

export default CartItems;