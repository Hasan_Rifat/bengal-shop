import Image from 'next/image';
import img1 from '../../../../../public/images/dealProduct3.png';
import img2 from '../../../../../public/images/dealProduct4.png';
import { IoIosAdd } from 'react-icons/io';
import { IoIosRemove } from 'react-icons/io';
import { MdDelete } from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { removeFromCart } from '../../../../../store/dealProductSlice/index';

const CartDetails = ({ product }) => {
  const dispatch = useDispatch();

  const {
    id,
    productImg,
    productName,
    category,
    currentPrice,
    previousPrice,
    subCategory,
    preSubCategory,
    ratings,
    numOfRatings,
    numOfCartProduct,
  } = product;
  return (
    <div className='grid grid-cols-4 p-2 border border-gray-300 mt-1 mb-5 justify-center items-center'>
      <div className='lg:w-[115px] lg:h-[115px] md:w-[80px] md:h-[80px] sm:w-[70px] sm:h-[70px] xs:w-[55px] xs:h-[55px] w-[40px] h-[40px] rounded-lg'>
        <Image
          src={productImg}
          width={115}
          height={115}
          alt='image not found'
          className='object-cover'
        ></Image>
      </div>
      <div className=''>
        <h1 className=' sm:font-semibold sm:text-sm lg:text-lg text-[14px] xs:font-normal'>
          {productName}
        </h1>
        <p>${currentPrice}</p>
        <p className='line-through'>${previousPrice}</p>
      </div>
      <div className='flex border border-gray-300 text-gray-600 divide-x divide-gray-300 lg:mr-[40px] sm:mr-[20px] xs:mr-[14px]'>
        <div className='h-8 w-8 text-xl flex items-center justify-center cursor-pointer select-none'>
          <i>
            <IoIosAdd />
          </i>
        </div>
        <div className='h-8 w-10 flex items-center justify-center'>
          {numOfCartProduct}
        </div>
        <div className='h-8 w-8 text-xl flex items-center justify-center cursor-pointer select-none'>
          <i>
            <IoIosRemove />
          </i>
        </div>
      </div>
      <div className='flex justify-between items-center'>
        <div className=''>
          <p className='xl:text-[25px] lg:text-[20px] text-[18px]'>
            ${numOfCartProduct * currentPrice}
          </p>
        </div>
        <div className='mr-[25%]'>
          <button
            className='smd:text-[25px] sm:text-[20px]'
            onClick={() => {
              dispatch(removeFromCart(id));
            }}
          >
            <i>
              <MdDelete />
            </i>
          </button>
        </div>
      </div>
    </div>
  );
};

export default CartDetails;
